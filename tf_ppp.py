#flow from SPSS being recreated here
#load data
#define target
#	Level2_Centrigues and Microcentrifuges_Price_Count > 0
#filter out fields
#	Level2_Centrigues and Microcentrifuges_Price_Count
#	Level2_Centrigues and Microcentrifuges_Price_Sum
#boost
#partition
#type
#feature selection
#tree model (C5)
#evaluation (98%)
#score


import pandas as pd
import numpy as np
import os
from sklearn.model_selection import KFold
from sklearn.model_selection import cross_val_score
from sklearn.pipeline import Pipeline
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeRegressor

def load_data():
    csv_path = "/home/jcroft/Python/jcLearn/tf360.csv"
    return pd.read_csv(csv_path)

tbl = load_data()
tbl["my_class"] = np.where(tbl["Level2_Centrifuges and Microcentrifuges_Price_Count"] > 0, 1, 0)
print(tbl["my_class"].value_counts())
del tbl["Level2_Centrifuges and Microcentrifuges_Price_Count"]
del tbl["Level2_Centrifuges and Microcentrifuges_Price_Sum"]
print(tbl.shape)

array = tbl.values
X = array[:,0:110]
Y = array[:,111]


# create pipeline
estimators = []
#estimators.append(('logistic', LogisticRegression()))
estimators.append(('decision_tree', DecisionTreeRegressor()))
model = Pipeline(estimators)

# evaluate pipeline
seed = 42
kfold = KFold(n_splits=10, random_state=seed)
results = cross_val_score(model, X, Y, cv=kfold)
print(results.mean())


