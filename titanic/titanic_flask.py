import pandas as pd
df = pd.read_csv('titanic2.csv')
include = ['Age','Sex','Embarked','Survived']
df_ = df[include]

categoricals = []
for col, col_type in df.dtypes.iteritems():
		if col_type == 'O':
				categoricals.append(col)
		#else:
		#df_[col].fillna(0, inplace=True)

df_ohe = pd.get_dummies(df, columns=categoricals, dummy_na=True)
df_ohe.fillna(0)

print(df_ohe.head())
df['Age'].fillna(0, inplace=True)
df_ohe.to_csv('out.csv')


# using a random forest classifier (can be any classifier)
##from sklearn.ensemble import RandomForestClassifier as rf
from sklearn.linear_model import LogisticRegression as lr
y = df_ohe['Survived']
x = df_ohe.drop('Survived', axis=1)
clf = lr()
clf.fit(x, y)

from sklearn.externals import joblib
joblib.dump(clf, 'model.pkl')


